import 'package:crud/models/todo.dart';
import 'package:flutter/material.dart';

class TodoCard extends StatelessWidget {
  final int index;
  final Todo item;
  final Function(Todo) navigateToEditPage;
  final Function(int) deleteById;

  const TodoCard(
      {super.key,
      required this.index,
      required this.item,
      required this.navigateToEditPage,
      required this.deleteById});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: CircleAvatar(child: Text('${index + 1}')),
        title: Text(item.title,
            style: TextStyle(
                decoration: item.isComplete
                    ? TextDecoration.lineThrough
                    : TextDecoration.none)),
        trailing: PopupMenuButton(
          onSelected: (value) {
            if (value == 'edit') {
              navigateToEditPage(item);
            } else if (value == 'delete') {
              deleteById(item.id);
            }
          },
          itemBuilder: (context) {
            return [
              const PopupMenuItem(value: 'edit', child: Text('Edit')),
              const PopupMenuItem(
                value: 'delete',
                child: Text('Delete'),
              )
            ];
          },
        ),
      ),
    );
  }
}
