import 'package:crud/api/todo_service.dart';
import 'package:crud/screens/add_page.dart';
import 'package:crud/utils/snackbar_helper.dart';
import 'package:crud/widget/todo_card.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../api/dio.dart';
import 'package:crud/models/todo.dart';

class TodoListPage extends StatefulWidget {
  const TodoListPage({super.key});

  @override
  State<TodoListPage> createState() => _TodoListPageState();
}

class _TodoListPageState extends State<TodoListPage> {
  bool isLoading = false;
  List<Todo> items = [];
  final API dio = API();

  @override
  void initState() {
    super.initState();
    fetchTodos();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Todo List'),
      ),
      body: Visibility(
        visible: isLoading,
        replacement: RefreshIndicator(
          onRefresh: fetchTodos,
          child: Visibility(
            visible: items.isNotEmpty,
            replacement: Center(
                child: Text('No Todo Item',
                    style: Theme.of(context).textTheme.headlineSmall)),
            child: ListView.builder(
              itemCount: items.length,
              padding: const EdgeInsets.all(8.0),
              itemBuilder: (context, index) {
                final item = items[index];
                return TodoCard(
                    index: index,
                    item: item,
                    navigateToEditPage: navigateToEditPage,
                    deleteById: deleteById);
              },
            ),
          ),
        ),
        child: const Center(child: CircularProgressIndicator()),
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: navigateToAddPage, label: const Text('Add Todo')),
    );
  }

  Future<void> navigateToAddPage() async {
    final route = MaterialPageRoute(builder: (context) => const AddTodoPage());
    await Navigator.push(context, route);

    setState(() {
      isLoading = true;
    });
    fetchTodos();
  }

  Future<void> navigateToEditPage(Todo item) async {
    final route =
        MaterialPageRoute(builder: (context) => AddTodoPage(todo: item));
    await Navigator.push(context, route);

    setState(() {
      isLoading = true;
    });
    fetchTodos();
  }

  Future<void> fetchTodos() async {
    setState(() {
      isLoading = true;
    });
    try {
      final todos = await TodoService.fetchTodos();

      setState(() {
        items = todos;
        isLoading = false;
      });
    } on DioError catch (ex) {
      print(ex.message.toString());
      showErrorMessage(context, message: ex.message.toString());
    }
  }

  Future<void> deleteById(int id) async {
    try {
      final result = await TodoService.deleteById(id);
      if (result > 0) {
        // fetchTodos();
        final filtered = items.where((el) => el.id != id).toList();
        setState(() {
          items = filtered;
        });
      }
    } on DioError catch (ex) {
      print(ex.message.toString());
      showErrorMessage(context, message: 'Could not delete this todo');
    }
  }
}
