import 'package:crud/api/todo_service.dart';
import 'package:crud/models/todo.dart';
import 'package:crud/utils/snackbar_helper.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import '../api/dio.dart';

class AddTodoPage extends StatefulWidget {
  final Todo? todo;
  const AddTodoPage({super.key, this.todo});

  @override
  State<AddTodoPage> createState() => _AddTodoPageState();
}

class _AddTodoPageState extends State<AddTodoPage> {
  TextEditingController titleController = TextEditingController();
  bool isComplete = false;

  bool isEdit = false;

  @override
  void initState() {
    super.initState();
    if (widget.todo != null) {
      isEdit = true;
      final title = widget.todo!.title;
      isComplete = widget.todo!.isComplete;
      titleController.text = title;
    }
  }

  @override
  Widget build(BuildContext context) {
    Future<void> updateData() async {
      final title = titleController.text;
      // final description = descriptionController.text;
      final body = {"title": title, 'isComplete': isComplete};

      try {
        final response = await TodoService.updateData(widget.todo!.id, body);

        showSuccussMessage(context, message: 'Update todo Success');
      } on DioError catch (ex) {
        print(ex.message);
        showErrorMessage(context, message: ex.message.toString());
      }
    }

    Future<void> onSubmit() async {
      final title = titleController.text;
      // final description = descriptionController.text;
      final body = {"title": title, 'isComplete': isComplete};

      try {
        final response = await TodoService.createTodo(body);
        titleController.text = '';
        isComplete = false;
        showSuccussMessage(context, message: 'Creation Success');
      } on DioError catch (ex) {
        print(ex.message);
        showErrorMessage(context, message: ex.message.toString());
      }
    }

    return Scaffold(
        appBar: AppBar(
          title: Text(isEdit ? 'Edit Todo' : 'Add Todo'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            children: [
              TextField(
                controller: titleController,
                decoration: const InputDecoration(hintText: ('Title')),
              ),
              SwitchListTile(
                title: const Text('Complete ?'),
                value: isComplete,
                onChanged: (newValue) {
                  setState(() {
                    isComplete = newValue;
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: isEdit ? updateData : onSubmit,
                  child: Text(isEdit ? 'Update' : 'Submit'))
            ],
          ),
        ));
  }
}
