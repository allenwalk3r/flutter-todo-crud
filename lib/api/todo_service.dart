import 'package:crud/api/dio.dart';

import 'package:crud/models/todo.dart';

class TodoService {
  static Future<int> deleteById(int id) async {
    final API dio = API();

    final String itemId = id.toString();

    final response = await dio.sendRequest.delete('/todos/$itemId');
    return response.data as int;
  }

  static Future<List<Todo>> fetchTodos() async {
    final API dio = API();

    final response = await dio.sendRequest.get('/todos');
    List<dynamic> result = response.data;
    return result.map((todoMap) => Todo.fromJson(todoMap)).toList();
  }

  static Future<List<dynamic>> updateData(int id, Map todo) async {
    final API dio = API();
    final response = await dio.sendRequest.put('/todos/$id', data: todo);
    List<dynamic> result = response.data;
    return result;
  }

  static Future<Map> createTodo(Map body) async {
    final API dio = API();
    final response = await dio.sendRequest.post('/todos/', data: body) as Map;
    return response;
  }
}
